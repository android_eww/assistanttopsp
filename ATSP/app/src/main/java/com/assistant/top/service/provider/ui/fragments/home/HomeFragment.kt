package com.assistant.top.service.provider.ui.fragments.home

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.data.Response.ChoosePlanResponse
import com.assistant.top.service.provider.data.Response.HomeDataResponse
import com.assistant.top.service.provider.databinding.DialogueFeedbackBinding
import com.assistant.top.service.provider.databinding.FragmentHomeBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.earningDetails.EarningDetailsActivity
import com.assistant.top.service.provider.ui.myRequestWithFragment.MyRequestsFragmentActivity
import com.assistant.top.service.provider.ui.myRequests.MyRequestsActivity
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.AppConstants
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistant.top.service.provider.utility.IntentKey
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), HomeFragmentNavigator {

    private val TAG = "HomeFragment"
    override val layoutId: Int get() = R.layout.fragment_home
    override val bindingVariable: Int get() = BR.viewmodel

    var homedataList : ArrayList<HomeDataResponse>  = ArrayList()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.setNavigator(this)
        curveChartData()
        mViewModel.homeData()

        binding.rvServiceBox.adapter = ServicesAdapter(mViewModel.getNavigator(),homedataList)
        AppLog.e(
            com.assistant.top.service.provider.extension.TAG,
            "userId=> ${userPreference.getUserId()}"
        )
        AppLog.e(
            com.assistant.top.service.provider.extension.TAG,
            "userName=> ${userPreference.getUserName()}"
        )

    }

    @SuppressLint("ResourceAsColor")
    fun curveChartData() {
        AppLog.e(TAG, "curveCharttt => $binding.curveChart")
        binding.curveChart.animateX(2500)
        binding.curveChart.xAxis?.setDrawGridLines(false)
        binding.curveChart.description?.text = ""
        binding.curveChart.legend?.isEnabled = false
        binding.curveChart.setTouchEnabled(false)
        binding.curveChart.isDragEnabled = false
        binding.curveChart.setScaleEnabled(false)
        binding.curveChart.setPinchZoom(false)
        binding.curveChart.setDrawGridBackground(false)
        binding.curveChart.getAxisLeft().setEnabled(false)
        binding.curveChart.getAxisRight().setEnabled(false)
        binding.curveChart.getLegend().setEnabled(false)
        binding.curveChart.getXAxis().setEnabled(false)

        binding.curveChart.maxHighlightDistance = 500f
        binding.curveChart.setViewPortOffsets(0f, 0f, 0f, 0f)
        lineChartDownFillWithData()

    }

    private fun lineChartDownFillWithData() {
        val description = Description()
        description.setText("Days Data")
        binding.curveChart.description = description

        val entryArrayList: ArrayList<Entry> = ArrayList()
        entryArrayList.add(Entry(0f, 60f, "0"))
        entryArrayList.add(Entry(1f, 55f, "1"))
        entryArrayList.add(Entry(2f, 40f, "2"))
        entryArrayList.add(Entry(3f, 30f, "3"))
        entryArrayList.add(Entry(4f, 96f, "3"))
        entryArrayList.add(Entry(5f, 45f, "3"))
        entryArrayList.add(Entry(6f, 39f, "3"))
        entryArrayList.add(Entry(7f, 49f, "3"))
        entryArrayList.add(Entry(8f, 52f, "3"))
        entryArrayList.add(Entry(9f, 80f, "3"))
        entryArrayList.add(Entry(10f, 95f, "3"))

        val lineDataSet = LineDataSet(entryArrayList, "This is y bill")
        lineDataSet.lineWidth = 3f
        lineDataSet.color = Color.parseColor("#FAB620")
        lineDataSet.highLightColor = Color.RED
        lineDataSet.setDrawValues(false)
        lineDataSet.circleRadius = 10f
        lineDataSet.setCircleColor(Color.YELLOW)

        lineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        lineDataSet.cubicIntensity = 0.3f

        //to fill the below of smooth line in graph
        lineDataSet.setDrawFilled(true)
        lineDataSet.fillColor = Color.YELLOW
        //set the transparency
        lineDataSet.fillAlpha = 100

        //set the gradiant then the above draw fill color will be replace
        val drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.gradient_drawable)
        lineDataSet.fillDrawable = drawable

        //set legend disable or enable to hide {the left down corner name of graph}
        val legend: Legend? = binding.curveChart.getLegend()
        legend?.isEnabled = false

        //to remove the cricle from the graph
        lineDataSet.setDrawCircles(false)

        //lineDataSet.setColor(ColorTemplate.COLORFUL_COLORS)
        val iLineDataSetArrayList: ArrayList<ILineDataSet> = ArrayList()
        iLineDataSetArrayList.add(lineDataSet)

        //LineData is the data accord
        val lineData = LineData(iLineDataSetArrayList)
        lineData.setValueTextSize(13f)
        lineData.setValueTextColor(Color.WHITE)
        binding.curveChart.data = lineData
        binding.curveChart.invalidate()
    }

    data class ChartModel(val s: String, val s1: String)

    override fun onItemClick(position: Int) {

        when (position) {
            0 -> requireActivity().startNewActivity(MyRequestsActivity::class.java)
            else -> {
                val bundle = Bundle()
                bundle.putInt(IntentKey.POSITION, (position - 1))
                requireActivity().startNewActivity(
                    MyRequestsFragmentActivity::class.java,
                    bundle = bundle
                )
            }
        }
    }

    override fun viewDetailClicked() {

        if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
            return
        }
        Constants.mLastClickTime = SystemClock.elapsedRealtime()

        requireActivity().startNewActivity(EarningDetailsActivity::class.java)
    }

    fun ratingDialog() {
        val dialog = Dialog(requireContext(), R.style.RoundedCornersDialogRating)
        var dialoginding = DialogueFeedbackBinding.inflate(LayoutInflater.from(requireContext()))

        dialog.setContentView(dialoginding.root)
        dialog.setCancelable(false)
        dialog.show()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val window: Window = dialog.window!!
        window.setLayout(
            ConstraintLayout.LayoutParams.MATCH_PARENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )

        dialoginding.btnSubmit.setCommonButtonListener {
            dialog.dismiss()
        }

        dialoginding.icCross.setOnClickListener {
            dialog.dismiss()
        }
    }

    override fun onResume() {
        super.onResume()
        binding.scrollView.pageScroll(View.SCROLL_INDICATOR_TOP)
    }

    override fun setupObservable() {
        mViewModel.getHomeDataObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    it.let {
                        val data = gson.toJson(it.data)
                        try {
                            val gson = Gson()
                            val topic = gson.fromJson(data, HomeDataResponse::class.java)
                            binding.txtAmount.text = topic.balance


                        }catch (e : Exception){

                        }
                        userPreference.saveUserSession(AppConstants.SharedPrefKey.USER_PREF, data)
                        Log.e("saveUserSession", "saveUserSession::${data}")
                    }
                }

                Resource.Status.ERROR -> {
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            requireActivity().alertDialog(message = if (checkIsConnectionReset(
                                    it.code
                                )
                            ) {
                                getString(R.string.connection_reset)
                            } else {
                                message
                            })

                        }
                        Log.e(TAG, "On Error ${it.message} ")
                    }

                }

                Resource.Status.LOADING -> {
                    Log.e("TAG", "On LOADING ${it.message} ")

                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e("TAG", "On NO_INTERNET_CONNECTION ${it.message} ")

                }
            }
        })
    }
}