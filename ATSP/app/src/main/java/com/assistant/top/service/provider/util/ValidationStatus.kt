package com.assistant.top.service.provider.util

import android.annotation.SuppressLint
import android.app.Activity
import android.util.Log
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.extension.getLanguage
import com.assistant.top.service.provider.language.KEY
import com.assistant.top.service.provider.utility.Constants.TAG
import com.assistentetop.customer.utils.alertDialog.showErrorAlert

enum class ValidationStatus {


    EMPTY_FIRSTNAME,
    TWOCHAR_FIRSTNAME,

    EMPTY_LASTNAME,
    TWOCHAR_LASTNAME,

    EMPTYEMAIL,
    InvalidEmail,

    PHONENUMBER,
    PHONENUMBERValid,
    GENDER,
    GENDERVALIDATE,

    PASSWORD,
    PASSWORDVALIDATE,
    ADDRESS_EMPTY,
    NIFID,
    ABOUTUS,

    TERMSCONDITION,
    PLEASE_SELECT_IMAGE,

    PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH,
    PASS_CONF_NOT_MATCH,
    EMPTY_CONFIRM_PASSWORD,
    EMPTY_CURRENT_PASSWORD,
    EMPTY_NEW_PASSWORD,
    OLD_PASSWORD_AND_NEW_PASSWORD_SAME,
    CHANGE_PASS_LENGTH_CURRENT,
    CHANGE_PASS_LENGTH_NEW,
    CHANGE_PASS_LENGTH_CONF_PASS,
    SPACE_PASSWORD_CURRENT,
    SPACE_PASSWORD_NEW,
    SPACE_PASSWORD,
    PASS_CONTAINS_MIDDLE_SPACE_CURRENT,
    PASS_CONTAINS_MIDDLE_SPACE_NEW,


}

object Validation {
    fun showMessageDialog(activity: Activity, validationStatus: ValidationStatus) {
        val message = getMessage(activity, validationStatus)
        if (message.isNotEmpty()) {
            activity.showErrorAlert(message = message)
            //activity.alertDialog(message = message)
        }
        AppLog.e(TAG,"Validation message=>${message.isNotEmpty()}")
    }

    private fun getMessage(activity: Activity, it: ValidationStatus): String {

        AppLog.e(TAG,"Validation messagemessage=>${activity.getLanguage(KEY.please_enter_a_valid_email)}")

        return when (it) {

            ValidationStatus.InvalidEmail -> activity.getLanguage(KEY.please_enter_a_valid_email)
            ValidationStatus.EMPTYEMAIL -> activity.getLanguage(KEY.please_enter_email)
            ValidationStatus.PASSWORD -> activity.getLanguage(KEY.please_enter_password)
            ValidationStatus.PASSWORDVALIDATE -> activity.getLanguage(KEY.minimum_8_characters_are_required)

            ValidationStatus.EMPTY_FIRSTNAME -> activity.getLanguage(KEY.please_enter_first_name)
            ValidationStatus.TWOCHAR_FIRSTNAME -> activity.getLanguage(KEY.first_name_minimum_2_characters_are_required)

            ValidationStatus.EMPTY_LASTNAME -> activity.getLanguage(KEY.please_enter_last_name)
            ValidationStatus.TWOCHAR_LASTNAME -> activity.getLanguage(KEY.last_name_minimum_2_characters_are_required)

            ValidationStatus.PHONENUMBER -> activity.getLanguage(KEY.please_enter_phone_number)
            ValidationStatus.PHONENUMBERValid -> activity.getLanguage(KEY.please_enter_10_digit_phone_number)

            ValidationStatus.GENDER -> activity.getLanguage(KEY.please_select_gender)
            ValidationStatus.GENDERVALIDATE -> activity.getLanguage(KEY.please_select_at_least_one_gender)
            ValidationStatus.ADDRESS_EMPTY -> activity.getLanguage(KEY.please_enter_address)
            ValidationStatus.NIFID -> activity.getLanguage(KEY.please_enter_nif_id)
            ValidationStatus.ABOUTUS -> activity.getLanguage(KEY.please_enter_about_us)

            ValidationStatus.TERMSCONDITION -> activity.getLanguage(KEY.please_select_terms_and__condition)
            ValidationStatus.PLEASE_SELECT_IMAGE -> activity.getLanguage(KEY.please_select_user_image)

            ValidationStatus.PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH -> activity.getLanguage(KEY.conform_password_not_match)
            ValidationStatus.PASS_CONF_NOT_MATCH -> activity.getLanguage(KEY.new_password_and_confirm_password_does_not_match)

            ValidationStatus.EMPTY_CONFIRM_PASSWORD -> activity.getLanguage(KEY.please_enter_confirm_password)
            ValidationStatus.EMPTY_CURRENT_PASSWORD -> activity.getLanguage(KEY.please_enter_current_password)
            ValidationStatus.EMPTY_NEW_PASSWORD -> activity.getLanguage(KEY.please_enter_new_password)


            ValidationStatus.OLD_PASSWORD_AND_NEW_PASSWORD_SAME -> activity.getLanguage(KEY.current_password_and_new_password_must_not_be_the_same)
            ValidationStatus.SPACE_PASSWORD -> activity.getLanguage(KEY.Space_should_not_allowed_before_and_after_the_entered_password)

            ValidationStatus.CHANGE_PASS_LENGTH_CURRENT -> activity.getLanguage(KEY.password_must_contain_at_least_8_characters_for_current_password)
            ValidationStatus.CHANGE_PASS_LENGTH_NEW -> activity.getLanguage(KEY.password_must_contain_atleast_8_characters_for_new_password)
            ValidationStatus.CHANGE_PASS_LENGTH_CONF_PASS -> activity.getLanguage(KEY.password_must_contain_at_least_8_characters_for_confirm_password)
            ValidationStatus.SPACE_PASSWORD_CURRENT -> activity.getLanguage(KEY.Space_should_not_allowed_before_and_after_the_entered_current_password)
            ValidationStatus.SPACE_PASSWORD_NEW -> activity.getLanguage(KEY.Space_should_not_allowed_before_and_after_the_entered_new_password)


            ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_CURRENT -> activity.getLanguage(KEY.Space_should_not_allow_in_current_password)
            ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_NEW -> activity.getLanguage(KEY.Space_should_not_allow_in_new_password)


            //ValidationStatus.EMPTYEMAIL -> activity.getString(R.string.please_enter_email)
//            ValidationStatus.PASSWORD -> activity.getString(R.string.please_enter_password)
//            ValidationStatus.PASSWORDVALIDATE -> activity.getString(R.string.please_enter_valid_password)


//            ValidationStatus.EMPTY_FIRSTNAME -> activity.getString(R.string.please_enter_firstname)
//            ValidationStatus.TWOCHAR_FIRSTNAME -> activity.getString(R.string.please_enter_twochar_firstname)
//
//            ValidationStatus.EMPTY_LASTNAME -> activity.getString(R.string.please_enter_lastname)
//            ValidationStatus.TWOCHAR_LASTNAME -> activity.getString(R.string.please_enter_twochar_lastname)
//
//            ValidationStatus.PHONENUMBER -> activity.getString(R.string.please_enter_phone_number)
//            ValidationStatus.PHONENUMBERValid -> activity.getString(R.string.please_enter_ten_digit_phone_number)
//
//            ValidationStatus.GENDER -> activity.getString(R.string.please_select_gender)
//            ValidationStatus.GENDERVALIDATE -> activity.getString(R.string.please_select_atleast_one_gender)
//
//            ValidationStatus.TERMSCONDITION -> activity.getString(R.string.please_select_checkbox)
//            ValidationStatus.PLEASE_SELECT_IMAGE -> activity.getString(R.string.please_select_user_image)

//            ValidationStatus.PASSWORD_AND_CONFIRM_PASSWORD_NOT_MATCH -> activity.getString(R.string.confirm_pwd_not_match)
//            ValidationStatus.PASS_CONF_NOT_MATCH -> activity.getString(R.string.pass_conf_same)
//            ValidationStatus.EMPTY_CONFIRM_PASSWORD -> activity.getString(R.string.please_enter_confirm_pwd)
//            ValidationStatus.EMPTY_CURRENT_PASSWORD -> activity.getString(R.string.enter_current_pass)
//            ValidationStatus.EMPTY_NEW_PASSWORD -> activity.getString(R.string.enter_new_pass)
//            ValidationStatus.OLD_PASSWORD_AND_NEW_PASSWORD_SAME -> activity.getString(R.string.current_and_new_must_notbe_same)
//            ValidationStatus.SPACE_PASSWORD -> activity.getString(R.string.password_space)
//            ValidationStatus.CHANGE_PASS_LENGTH_CURRENT -> activity.getString(R.string.change_password_invalid_current)
//            ValidationStatus.CHANGE_PASS_LENGTH_NEW -> activity.getString(R.string.change_password_invalid_new)
//            ValidationStatus.CHANGE_PASS_LENGTH_CONF_PASS -> activity.getString(R.string.change_password_invalid_new_conf_pass)
//            ValidationStatus.SPACE_PASSWORD_CURRENT -> activity.getString(R.string.password_space_current)
//            ValidationStatus.SPACE_PASSWORD_NEW -> activity.getString(R.string.password_space_new)
//            ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_CURRENT -> activity.getString(R.string.password_space_middle_current)
//            ValidationStatus.PASS_CONTAINS_MIDDLE_SPACE_NEW -> activity.getString(R.string.password_space_middle_new)


        }
    }
}