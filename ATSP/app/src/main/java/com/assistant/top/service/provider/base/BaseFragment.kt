package com.assistant.top.service.provider.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.language.Language
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.util.network.AppConstants
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.google.gson.Gson
import javax.inject.Inject

abstract class BaseFragment<T : ViewDataBinding, V : ViewModel> : Fragment() {

    @Inject
    lateinit var mViewModel: V
    lateinit var binding: T


    lateinit var userPreference: UserPreference

    companion object {
        var language: Language? = null
    }


    @Inject
    lateinit var activity: Activity

    abstract val layoutId: Int
    abstract val bindingVariable: Int

    @Inject
    lateinit var gson: Gson

    abstract fun setupObservable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        performDataBinding(inflater, container!!)
        userPreference =
            UserPreference(
                requireContext().getSharedPreferences(
                    AppConstants.USER_PREFERENCE,
                    Context.MODE_PRIVATE), requireContext()
            )
        language = Language.newInstance(requireActivity())
        return binding.root
    }

    open fun onBackPressed() {
        requireActivity().onBackPressed()
    }

    private fun performDataBinding(inflater: LayoutInflater, container: ViewGroup) {
        Log.e("layoutId","layoutId = {$layoutId}")
        binding = DataBindingUtil.inflate(inflater,layoutId, container, false)
        binding.setVariable(bindingVariable, mViewModel)
        binding.executePendingBindings()
        activity=requireActivity()
        setupObservable()

    }

    fun startActivityWithFinish(target: Class<*>, bundle: Bundle?) {
        //Call new activity with finish current activity
        val intent = Intent(requireActivity(), target)
        bundle?.let { intent.putExtras(it) }
        startActivity(intent)
        requireActivity().finish()
    }

    open fun checkIsSessionOut(code: Int): Boolean {
        if (code == 403) {
            userPreference.clearUserSession()
            requireActivity().alertDialog(message = getString(R.string.session_expired), positiveClick = {
                startActivityWithFinish(LoginActivity::class.java,bundle = null)
            })
        }
        return code == 403
    }

    open fun checkIsConnectionReset(code: Int): Boolean {
        return code == 502
    }

}