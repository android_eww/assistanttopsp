package com.assistant.top.service.provider.ui.fragments.manageAvalibility

interface ManageAvailabilityNavigator {

    fun onClickDayChange()
}