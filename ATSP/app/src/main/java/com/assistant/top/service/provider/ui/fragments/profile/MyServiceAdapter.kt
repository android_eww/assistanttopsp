package com.assistant.top.service.provider.ui.fragments.profile

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.MyServiceItemListBinding


class MyServiceAdapter : RecyclerView.Adapter<MyServiceAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: MyServiceItemListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.my_service_item_list,
            parent,
            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

    }

    override fun getItemCount(): Int {
        return 5
    }

    class MenuHolder(var binding: MyServiceItemListBinding) : RecyclerView.ViewHolder(binding.root)
}