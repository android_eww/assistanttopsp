package com.assistant.top.service.provider.util

import android.util.Log
import com.assistant.top.service.provider.BuildConfig

object AppLog {

    fun e(tag: String, msg: String) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg)
        }
    }

    fun d(tag: String?, msg: String?) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg!!)
        }
    }
}