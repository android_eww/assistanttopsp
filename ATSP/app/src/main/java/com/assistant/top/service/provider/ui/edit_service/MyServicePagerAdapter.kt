package com.assistant.top.service.provider.ui.edit_service

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.assistant.top.service.provider.ui.edit_service.appointment.AppointMentFragment
import com.assistant.top.service.provider.ui.edit_service.general.GeneralFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.cancelled.CancelledFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.completed.CompletedFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.inProgress.InProgressFragment
import com.assistant.top.service.provider.ui.myRequestWithFragment.pending.PendingFragment


class MyServicePagerAdapter(fragmentManager: FragmentManager?) : FragmentPagerAdapter(fragmentManager!!) {
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    // Returns the fragment to display for that page
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> GeneralFragment()
            1 -> AppointMentFragment()
            else -> GeneralFragment()
        }
    }

    // Returns the page title for the top indicator
    override fun getPageTitle(position: Int): CharSequence {
        return "Page $position"
    }

    companion object {
        private const val NUM_ITEMS = 2
    }
}