package com.assistant.top.service.provider.extension

import android.app.Activity
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.util.AppLog
import java.util.regex.Matcher
import java.util.regex.Pattern

fun String.emailValid(): Boolean {
    val EMAIL_PATTERN =
        "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
    val pattern = Pattern.compile(EMAIL_PATTERN)
    val matcher: Matcher = pattern.matcher(this)
    return matcher.matches()
}

fun Activity.getLanguage(key : String): String {

    if (BaseActivity.language?.getLanguage(key) != null){
        AppLog.e(TAG,"languageDataNull=> ${BaseActivity.language?.getLanguage(key).toString()}")
        AppLog.e(TAG,"key Language=> $key")
        return BaseActivity.language?.getLanguage(key).toString()
    }
    return key.replace("_"," ")
}