package com.assistant.top.service.provider.ui.home

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.BaseResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.ENDPOINT_LOGOUT
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val networkService: NetworkService,
    private val context: Context,
    private val userPreference: UserPreference,
) : BaseViewModel<HomeActivityNavigator>() {

        fun onClickLogout(){
            getNavigator()?.onLogout()
        }

    private val logoutObservable: MutableLiveData<Resource<BaseResponse>> = MutableLiveData()

    fun getLogoutObservable(): LiveData<Resource<BaseResponse>> {
        return logoutObservable
    }

    fun logout() {
        val userId = userPreference.getUserId()
        if (NetworkUtils.isNetworkConnected(context)) {
            var response: Response<BaseResponse>? = null
            viewModelScope.launch {
                logoutObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    response = networkService.logout(ENDPOINT_LOGOUT.plus(userId))
                }

                withContext(Dispatchers.Main) {
                    response.run {
                        logoutObservable.value = baseDataSource.getResult { this!! }
                    }
                }
            }
        } else {
            Log.e("nointernet", "....")
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    logoutObservable.value = Resource.noInternetConnection(null)
                }

            }

        }
    }
}