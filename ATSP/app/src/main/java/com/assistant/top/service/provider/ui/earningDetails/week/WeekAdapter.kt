package com.assistant.top.service.provider.ui.earningDetails.week

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.PendingServiceListItemBinding
import com.assistant.top.service.provider.databinding.TimeDurationListItemBinding

class WeekAdapter(val requireActivity: Context) :
    RecyclerView.Adapter<WeekAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: TimeDurationListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.time_duration_list_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.itemView.setOnClickListener {
        }
    }

    override fun getItemCount(): Int {
        return 2
    }

    class MenuHolder(var binding: TimeDurationListItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}