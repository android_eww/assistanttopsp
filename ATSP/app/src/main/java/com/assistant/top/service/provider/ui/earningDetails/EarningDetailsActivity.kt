package com.assistant.top.service.provider.ui.earningDetails

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager.widget.ViewPager
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityEarningDetailsBinding
import com.assistant.top.service.provider.ui.myRequestWithFragment.MyPagerAdapter
import com.assistant.top.service.provider.ui.myRequestWithFragment.TabSelectionAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EarningDetailsActivity : BaseActivity<ActivityEarningDetailsBinding,EarningDetailsViewModel>(),EarningNavigator {


    private val position: Int = 0
    private lateinit var adapterViewPager: PagerAdapter
    private lateinit var tabAdapter: TimeDurationAdapter
    override val layoutId: Int get() = R.layout.activity_earning_details
    override val bindingVariable: Int get() = BR.viewmodel

    var tabList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initt()

        binding.mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                tabAdapter.scrollTo(position)
                binding.rvTab.scrollToPosition(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }

    private fun initt() {
        mViewModel.setNavigator(this)

        tabList.add(getString(R.string.all))
        tabList.add(getString(R.string.today))
        tabList.add(getString(R.string.yesterday))
        tabList.add(getString(R.string.week))


        tabAdapter = TimeDurationAdapter(mViewModel.getNavigator(),tabList,0)
        binding.rvTab.adapter = tabAdapter

        adapterViewPager = PagerAdapter(supportFragmentManager)
        binding.mViewPager.adapter = adapterViewPager

        binding.rvTab.scrollToPosition(position)
        onItemClick(position)
    }


    override fun onItemClick(position : Int) {
        binding.mViewPager.currentItem = position
    }

    override fun onBackPressed() {
        finish()
    }

    override fun setupObservable() {

    }

}