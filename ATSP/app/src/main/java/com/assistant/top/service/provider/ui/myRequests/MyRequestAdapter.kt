package com.assistant.top.service.provider.ui.myRequests

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.MyRequestListItemBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.myRequestWithFragment.MyRequestsFragmentActivity
import com.assistant.top.service.provider.ui.service_details.ServiceDetailsActivity
import com.assistant.top.service.provider.utility.Constants
import com.assistant.top.service.provider.utility.IntentKey

class MyRequestAdapter(val myRequestsActivity: MyRequestsActivity) : RecyclerView.Adapter<MyRequestAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: MyRequestListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.my_request_list_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {


        when (position) {
            0 -> {
                setData(holder, "House", R.drawable.ic_doctor)
            }
            1 -> {
                setData(holder, "Construction", R.drawable.ic_doctor)
            }
            2 -> {
                setData(holder, "Auto Mechanic", R.drawable.ic_doctor)
            }
            3 -> {
                setData(holder, "Fitness Club", R.drawable.ic_doctor)
            }
            4 -> {
                setData(holder, "Doctors", R.drawable.ic_doctor)
            }
        }

        holder.itemView.setOnClickListener {

            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000){
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()

            val bundle = Bundle()
            bundle.putInt(IntentKey.POSITION, 0)
            myRequestsActivity.startNewActivity(
                ServiceDetailsActivity::class.java,
                bundle = bundle
            )
        }
    }

    private fun setData(holder: MenuHolder, serviceName: String, resource: Int) {


        //holder.binding.serviceIcon.setImageResource(resource)
        //holder.binding.serviceName.text = serviceName
    }

    override fun getItemCount(): Int {
        return 5
    }

    class MenuHolder(var binding: MyRequestListItemBinding) : RecyclerView.ViewHolder(binding.root)
}