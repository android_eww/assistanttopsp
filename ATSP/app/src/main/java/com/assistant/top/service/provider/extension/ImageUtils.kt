package com.assistant.top.service.provider.extension

import android.widget.ImageView
import com.assistant.top.service.provider.R
import com.bumptech.glide.Glide

fun ImageView.loadImage(path: String, placeholder: Int = R.drawable.ic_launcher_background) {
    Glide.with(context)
        .load(path)
        .placeholder(placeholder)
        .error(placeholder)
        .into(this)
}
