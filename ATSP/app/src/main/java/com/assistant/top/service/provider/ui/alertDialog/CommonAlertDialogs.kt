package com.assistentetop.customer.utils.alertDialog


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.ui.view.SnacyAlert


fun Context.alertDialog(
    title: String = resources.getString(R.string.app_name),
    message: String = " ",
    isCancelable: Boolean = false,
    positiveBtnText: String = resources.getString(R.string.ok),
    negativeBtnText: String? = null,
    negativeClick: (() -> Unit)? = null,
    positiveClick: (() -> Unit)? = null,
    positiveBtnTextColor: Boolean = false,
) {
    val builder = AlertDialog.Builder(this)
    builder.setTitle(title)
    builder.setMessage(message)
    builder.setCancelable(isCancelable)

    builder.setPositiveButton(positiveBtnText) { _, _ -> positiveClick?.invoke() }

    negativeBtnText?.let {
        builder.setNegativeButton(negativeBtnText) { _, _ -> negativeClick?.invoke() }
    }

    val alertDialog = builder.create()
    alertDialog.show()
    //  if (positiveBtnTextColor) {
    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
        .setTextColor(resources.getColor(R.color.dark_blue))
    /* } else {

         alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
             .setTextColor(resources.getColor(R.color.chat_color_gray))
     }*/
    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
        .setTextColor(resources.getColor(R.color.grey))

}

fun Activity.showErrorAlert(
    message: String = " ",
    title: String = getString(R.string.required),
    bgColor: Int = R.color.yellow,
    icon: Int = R.drawable.ic_baseline_required_24,
) {
    SnacyAlert.create(this)
        .setText(message)
        .setTitle(title)
        .setBackgroundColorRes(bgColor)
        .setDuration(1500)
        .showIcon(true)
        .setIcon(icon)
        .show()
}

fun Activity.showMobileAlreadyRegister(
    message: String = getString(R.string.mobile_number_already_exist),
    title: String = getString(R.string.error),
    bgColor: Int = R.color.red,
    icon: Int = R.drawable.ic_cross,
) {
    SnacyAlert.create(this)
        .setText(message)
        .setTitle(title)
        .setBackgroundColorRes(bgColor)
        .setDuration(2500)
        .showIcon(true)
        .setIcon(icon)
        .show()

}

fun Activity.showSuccessAlert(message: String = "") {
    SnacyAlert.create(this)
        .setText(message)
        .setTitle(getString(R.string.success))
        .setBackgroundColorRes(R.color.green)
        .setDuration(1500)
        .showIcon(true)
        .setIcon(R.drawable.ic_round_check_circle)
        .show()
}

fun Fragment.showSuccessAlert(message: String = " ") {
    SnacyAlert.create(requireActivity())
        .setText(message)
        .setTitle(getString(R.string.success))
        .setBackgroundColorRes(R.color.green)
        .setDuration(1500)
        .showIcon(true)
        .setIcon(R.drawable.ic_round_check_circle)
        .show()
}

@SuppressLint("UseCompatLoadingForDrawables")
fun Activity.showInternetDialog(
    isCancelAble: Boolean = false,
    positiveClick: (() -> Unit)? = null,
) {
    showErrorAlert(
        this.getString(R.string.network_connection_error),
        title = getString(R.string.error),
        bgColor = R.color.yellow,
        icon = R.drawable.ic_round_cancel
    )
    val builder = AlertDialog.Builder(this)
    builder.setTitle(this.getString(R.string.app_name))
    builder.setMessage(this.getString(R.string.network_connection_error))
    builder.setPositiveButton(this.getString(R.string.ok)){_,_, ->
        positiveClick?.invoke()
    }
    builder.setCancelable(isCancelAble)
    val alertDialog = builder.create()
    alertDialog.setOnShowListener {
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.yellow))

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(ContextCompat.getColor(this, R.color.yellow))
    }
    alertDialog.show()

}

fun Context.checkLocationPermissionCommon(): Boolean {
    return ContextCompat.checkSelfPermission(
        applicationContext,
        Manifest.permission.ACCESS_COARSE_LOCATION
    ) + ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.ACCESS_FINE_LOCATION
    ) != PackageManager.PERMISSION_GRANTED
}

fun Context.requestLocationPermission(REQUEST_CODE: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ActivityCompat.requestPermissions(
            this as Activity, arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ), REQUEST_CODE
        )
    }
}


fun Context.checkImagePermission(): Boolean {
    return ContextCompat.checkSelfPermission(
        applicationContext,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) + ContextCompat.checkSelfPermission(
        this,
        Manifest.permission.CAMERA
    ) != PackageManager.PERMISSION_GRANTED
}

fun Context.checkImageGalleryPermission(): Boolean {
    return ActivityCompat.checkSelfPermission(
        applicationContext,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) != PackageManager.PERMISSION_GRANTED

}

fun Fragment.requestImagePermissionFragment(REQUEST_CODE: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        requestPermissions(
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ), REQUEST_CODE
        )
    }
}

fun Fragment.requestImageGalleryPermissionFragment(REQUEST_CODE: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        requestPermissions(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ), REQUEST_CODE
        )
    }
}

fun Context.requestImageGalleryPermission(REQUEST_CODE: Int) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        ActivityCompat.requestPermissions(
            this as Activity, arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE
            ), REQUEST_CODE
        )
    }
}


fun Activity.showErrorMsg502(code: Int, message: String) {
    showErrorAlert(
        message = if (checkIsConnectionReset(code)) {
            getString(R.string.connection_reset)
        } else {
            message
        },
        title = getString(R.string.error),
        bgColor = R.color.red,
        icon = R.drawable.ic_round_cancel
    )
}

fun checkIsConnectionReset(code: Int): Boolean {
    return code == 502
}

fun Context.showLocationPermissionNeeded() {
    alertDialog(message = getString(R.string.permission_needed), positiveClick = {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", packageName, null)
        intent.data = uri
        startActivity(intent)
    })
}




