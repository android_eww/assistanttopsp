package com.assistant.top.service.provider.ui.home

import android.app.Activity

data class MenuItemModel(var itemName: String, var image: Int)
