package com.assistant.top.service.provider.ui.splash

import android.content.Context
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.BuildConfig
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.InitResponse
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.API_CONSTANTS.Companion.ENDPOINT_INIT
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants.TAG
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    var context: Context,
    var networkService: NetworkService,
    var userPreference: UserPreference
) : BaseViewModel<Any>(
) {
    private var jsonString: String? = null
    var mStatus: Boolean = false
    var mupdate: Boolean = false
    var mMaintenance: Boolean = false
    var mtxtAmount: Int = 0
    var mtaxStatus: String = ""
    var mtaxType: String = ""
    var code: String = "en"

    private val initResponseObservable: MutableLiveData<Resource<InitResponse>> = MutableLiveData()

    fun getInitObservable(): LiveData<Resource<InitResponse>> {
        return initResponseObservable
    }

    private val showOptionalUpdateDialog: MutableLiveData<String> = MutableLiveData()

    fun getOptionUpdateObservable(): LiveData<String> {
        return showOptionalUpdateDialog
    }

    private val showCompulsoryUpdateDialog: MutableLiveData<String> = MutableLiveData()

    fun getCompulsoryUpdateObservable(): LiveData<String> {
        return showCompulsoryUpdateDialog
    }

    private val showMaintenanceDialog: MutableLiveData<String> = MutableLiveData()

    fun getMaintenanceObservable(): LiveData<String> {
        return showMaintenanceDialog
    }

    private val launchHome: MutableLiveData<String> = MutableLiveData()

    fun geLaunchHomeObservable(): LiveData<String> {
        return launchHome
    }

    private val launchLogin: MutableLiveData<String> = MutableLiveData()

    fun geLaunchLoginObservable(): LiveData<String> {
        return launchLogin
    }

    private val languageLabelResponseObservable: MutableLiveData<Resource<String>> =
        MutableLiveData()

    fun getLanguageLabelObservable(): LiveData<Resource<String>> {
        return languageLabelResponseObservable
    }


    fun lagunageLabel() {
        AppLog.e(TAG, "responselagunageLabel")

        if (NetworkUtils.isNetworkConnected(context)) {


            lateinit var response: JsonObject
            viewModelScope.launch {
                // languageLabelResponseObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    response = networkService.languageLabel(code)
                    jsonString = Gson().toJson(response)
                    AppLog.e(TAG, "response =>> $response")
                    AppLog.e(TAG, "jsonString =>> $jsonString")

                    try {

                        val jsonObject = JSONObject(jsonString)
                        language?.languageCode = jsonObject.getString("language_code")
                        AppLog.e(TAG,"language_code => ${jsonObject.getString("language_code")}")


                        if (jsonObject.has("labels")){
                            val languageObject = jsonObject["labels"]
                            AppLog.e(TAG,"languageObject lable => $languageObject")
                            language?.setLanguage(languageObject.toString())
                        }

                        AppLog.e(TAG,"language => ${language.toString()}")


                    }catch (e: Exception){
                        AppLog.e(TAG,"language lable exception => ${e.toString()}")
                    }

                }

                /*  withContext(Dispatchers.Main) {
                      response.run {
                          languageLabelResponseObservable.value = baseDataSource.getResult { jsonString as Response<T> }
                      }
                  }*/
            }

        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    languageLabelResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }

    fun getRequestBody(content: String): RequestBody {
        return RequestBody.create("multipart/form-data".toMediaTypeOrNull(), content)
    }

    fun initApi() {

        if (NetworkUtils.isNetworkConnected(context)) {
            var response: Response<InitResponse>? = null
            viewModelScope.launch {
                initResponseObservable.value = Resource.loading(null)
                withContext(Dispatchers.IO) {
                    AppLog.e(TAG, "$response")
                    response = networkService.init(ENDPOINT_INIT.plus(BuildConfig.VERSION_NAME))
                }
                withContext(Dispatchers.Main) {
                    response.run {
                        AppLog.e(TAG, "${baseDataSource.getResult(true) { this!! }}")
                        initResponseObservable.value = baseDataSource.getResult(true) { this!! }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    initResponseObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }


    fun checkApiFlags(res: InitResponse) {
        mStatus = res.status
        mMaintenance = res.maintenance
        mupdate = res.update
        mtxtAmount = res.tax_amount
        mtaxStatus = res.tax_status
        mtaxType = res.tax_type

        AppLog.e(TAG, "mStatus => $mStatus")
        AppLog.e(TAG, "mMaintenance => $mMaintenance")
        AppLog.e(TAG, "mupdate => $mupdate")
        AppLog.e(TAG, "mtaxStatus => $mtaxStatus")
        AppLog.e(TAG, "mtxtAmount => $mtxtAmount")
        AppLog.e(TAG, "mtaxType => $mtaxType")
        AppLog.e(TAG, "mtaxStatus => $mtaxStatus")

        if (isFlagCheckRequired(res.message)) {
            checkAppFlag(res.message)
        } else {
            goToNextScreen()
        }
    }

    private fun isFlagCheckRequired(message: String): Boolean {
        return !TextUtils.isEmpty(message)
    }


    private fun checkAppFlag(message: String) {
        AppLog.d(TAG, "checkAppFlag called: message is:= $message")
        if (isAppInMaintenance()) {
            showMaintenanceDialog(message)

            return
        }
        if (isUpdateRequired()) {
            showUpdateRequiredDialog(message)
            return
        }
        if (isOptionalUpdateAvailable()) {
            showOptionalUpdateDialog(message)
        }
    }


    private fun showOptionalUpdateDialog(message: String) {
        showOptionalUpdateDialog.postValue(message)
    }

    private fun showUpdateRequiredDialog(message: String) {
        showCompulsoryUpdateDialog.postValue(message)
    }

    private fun showMaintenanceDialog(message: String) {
        showMaintenanceDialog.postValue(message)
    }

    private fun isUpdateRequired(): Boolean {
        AppLog.d(
            "TAG",
            "isUpdateRequired called = compulsory update = " + (!mStatus && mupdate)
        )
        return !mStatus && mupdate
    }

    private fun isAppInMaintenance(): Boolean {
        AppLog.d(
            "TAG",
            "isAppInMaintenance called: app in maintenance = maintenance =$mMaintenance"
        )
        return mMaintenance
    }

    private fun isOptionalUpdateAvailable(): Boolean {
        AppLog.d(
            "TAG",
            "isOptionalUpdateAvailable called: optional update =" + (!mStatus && !mupdate)
        )
        return mStatus && !mupdate
    }

    fun goToNextScreen() {
        if (userPreference.isUserLogin()) {
            AppLog.d("home screen", "goToNextScreen home screen")
            launchHome.postValue("")
        } else {
            AppLog.d("login screen", "goToNextScreen login screen")
            launchLogin.postValue("")
        }
    }

}