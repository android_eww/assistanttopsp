package com.assistant.top.service.provider.util.network

interface NetworkHelper {

    fun isNetworkConnected(): Boolean
    fun castToNetworkError(throwable: Throwable?): NetworkError?

}