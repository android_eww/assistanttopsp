package com.assistant.top.service.provider.ui.chat

import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModel
import com.assistant.top.service.provider.base.BaseViewModel
import javax.inject.Inject

class ChatViewModel @Inject constructor() : BaseViewModel<ChatNavigator>() {

    fun onClickSend() {
        getNavigator()?.onSendClicked()
    }

}