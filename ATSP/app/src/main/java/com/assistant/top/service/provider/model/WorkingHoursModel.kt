package com.assistant.top.service.provider.model

data class WorkingHoursModel(var days : String,var timingList : ArrayList<TimingList> = arrayListOf())
data class TimingList(var startTime:String,var endTime:String,var isChecked:Boolean)