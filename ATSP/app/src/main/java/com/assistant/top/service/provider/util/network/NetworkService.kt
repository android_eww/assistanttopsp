package com.assistant.top.service.provider.util.network

import com.assistant.top.service.provider.data.Response.*
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface NetworkService {

    @GET
    suspend fun init(@Url url: String): Response<InitResponse>

    @FormUrlEncoded
    @POST(API_CONSTANTS.WEB_SERVICE_LANGUAGE_LABEL)
    suspend fun languageLabel(
        @Field(API_CONSTANTS.WEB_PARAM_LANGUAGE_LABEL_CODE) languageLabel: String): JsonObject


    @FormUrlEncoded
    @POST(API_CONSTANTS.WEB_SERVICE_LOGIN)
    suspend fun login(@FieldMap paramLogin: Map<String, String>): Response<LoginResponse>

    @Multipart
    @POST(API_CONSTANTS.WEB_SERVICE_REGISTER)
    suspend fun register(
        @PartMap paramLogin: @JvmSuppressWildcards Map<String, RequestBody>,
        @Part image: MultipartBody.Part? = null
    ): Response<LoginResponse>

    @FormUrlEncoded
    @POST(API_CONSTANTS.WEB_SERVICE_FORGOT)
    suspend fun forgotPwd(@FieldMap paramLogin: Map<String, String>): Response<BaseResponse>


    @FormUrlEncoded
    @POST(API_CONSTANTS.WEB_SERVICE_CHANGE_PWD)
    suspend fun changePwd(@FieldMap paramLogin: Map<String, String>): Response<BaseResponse>

    @Multipart
    @POST(API_CONSTANTS.WEB_SERVICE_PROFILE_UPDATE)
    suspend fun updateProfileData(
        @PartMap paramUpdateProfile: @JvmSuppressWildcards Map<String, RequestBody>,
        @Part image: MultipartBody.Part? = null,
    ): Response<ProfileDataResponse>

    @GET
    suspend fun addServices(@Url url: String): Response<AddServicesResponse>

    @GET
    suspend fun choosePlan(@Url url: String): Response<ChoosePlanResponse>

    @GET
    suspend fun logout(@Url url: String): Response<BaseResponse>


    @FormUrlEncoded
    @POST(API_CONSTANTS.WEB_SERVICE_HOME_DATA)
    suspend fun homeData(@FieldMap paramHomeData: Map<String, String>): Response<HomeDataResponse>


}