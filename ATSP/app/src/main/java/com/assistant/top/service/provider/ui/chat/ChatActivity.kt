package com.assistant.top.service.provider.ui.chat

import android.os.Bundle
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityChatBinding
import com.assistant.top.service.provider.extension.hideKeyboard
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChatActivity : BaseActivity<ActivityChatBinding,ChatViewModel>(),ChatNavigator {

    override val layoutId: Int get() = R.layout.activity_chat
    override val bindingVariable: Int get() = BR.viewmodel

    var chatList  = mutableListOf<ChatModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setNavigator(this)
        binding.commonToolbar.binding.ivBack.setOnClickListener {
            onBackPressed()
        }

        chatList.add(ChatModel("Hey! How's it going?",0,"Today at 5:03 PM"))
        chatList.add(ChatModel("Great thanks, I'm just finished the design for tomorrow's presentation.",1,""))
        chatList.add(ChatModel("Would you like to go the coffee tonight?",0,""))
        chatList.add(ChatModel("OK, sure !",1,"Today at 5:15 PM"))
        chatList.add(ChatModel("When do you want to go?",1,""))
        chatList.add(ChatModel("bye Good Night",0,""))
        chatList.add(ChatModel("Take care",0,""))
        chatList.add(ChatModel("Good Night! bye",1,"Today at 5:45 PM"))

        binding.rvChat.adapter = ChatAdapter(chatList,this)
        binding.rvChat.scrollToPosition(chatList.size-1)

    }

    override fun onSendClicked() {
        //hideKeyboard()
        binding.edtFirstName.setText("")
    }

    override fun setupObservable() {

    }

}