package com.assistant.top.service.provider.ui.home

interface HomeActivityNavigator {

    fun onClickDrawerItem(position : Int)
    fun onProfileFragmentClicks()
    fun onLogout()

}