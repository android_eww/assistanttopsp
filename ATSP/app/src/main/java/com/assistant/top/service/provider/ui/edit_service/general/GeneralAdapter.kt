package com.assistant.top.service.provider.ui.edit_service.general

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.EditServiceItemListBinding
import com.assistant.top.service.provider.databinding.PendingServiceListItemBinding
import com.assistant.top.service.provider.extension.setRectDrawable

class GeneralAdapter(val requireActivity: FragmentActivity) :
    RecyclerView.Adapter<GeneralAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: EditServiceItemListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.edit_service_item_list,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {


    }


    override fun getItemCount(): Int {
        return 1
    }

    class MenuHolder(var binding: EditServiceItemListBinding) :
        RecyclerView.ViewHolder(binding.root)
}