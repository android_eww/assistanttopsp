package com.assistant.top.service.provider.util

import android.content.Context
import android.util.Patterns
import android.view.View
import com.assistant.top.service.provider.ui.alertDialog.LoadingDialog

fun String.isEmailValid(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isNameValid(): Boolean {
    return this.length > 1
}

fun String.isPhoneValid(): Boolean {
    return this.length > 9
}

fun String.ispasswordValid(): Boolean {
    return this.length > 7
}

fun String.isSpacePassword(): Boolean {
    return !(this.get(0) == ' ' || this.get(this.length - 1) == ' ')
}

fun String.isSpacePasswordContains(): Boolean {
    return !(this.contains(" "))
}

fun Context.showLoaderDialog() {
    LoadingDialog.showLoadDialog(this)
}

fun Context.hideLoaderDialog() {
    LoadingDialog.hideLoadDialog()
}

fun Context.isLoaderShowing(): Boolean {
    return LoadingDialog.isDialogShowing()
}

fun delayOnClick(view: View) {
    view.setEnabled(false)
    view.postDelayed(Runnable { view.setEnabled(true) }, 1000)

}
/*fun EditText.setTextWatcherView() {
    this.addTextChangedListener(GenericTextWatcher(this))
}*/






