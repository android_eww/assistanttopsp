package com.assistant.top.service.provider.ui.view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.res.ResourcesCompat
import com.assistant.top.service.provider.R

class MVTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : AppCompatTextView(context, attrs, defStyle) {
    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MVTextView)
        val textStyle = a.getInt(R.styleable.MVTextView_textStyle, 1)
        typeface = getTypeface(context, textStyle)
        a.recycle()
    }

    private fun getTypeface(context: Context, textStyle: Int): Typeface? {
        when (textStyle) {
            0 -> {
                return ResourcesCompat.getFont(context, R.font.aileron_regular)
            }
            1 -> {
                return ResourcesCompat.getFont(context, R.font.aileron_semibold)
            }
            2 -> {
                return ResourcesCompat.getFont(context, R.font.aileron_black)
            }
            3 -> {
                return ResourcesCompat.getFont(context, R.font.aileron_light)
            }
            4->{
                return ResourcesCompat.getFont(context, R.font.aileron_black)
            }
        }
        return ResourcesCompat.getFont(context, R.font.aileron_regular)
    }
}