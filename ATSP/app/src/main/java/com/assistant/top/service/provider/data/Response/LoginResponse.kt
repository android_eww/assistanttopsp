package com.assistant.top.service.provider.data.Response

import com.google.gson.annotations.SerializedName

data class LoginResponse(

    var progressButtonText: String = "",

    @SerializedName("user_data")
    val user_data: Data,

    @SerializedName("x-api-key")
    val xApiKey: String,

    ) : BaseResponse() {

    data class Data(
        @SerializedName("id")
        val id: String,

        @SerializedName("first_name")
        val first_name: String,

        @SerializedName("last_name")
        val last_name: String,

        @SerializedName("full_name")
        val full_name: String,

        @SerializedName("email")
        val email: String,

        @SerializedName("gender")
        val gender: String,

        @SerializedName("mobile_number")
        val mobile_number: String,

        @SerializedName("address")
        val address: String,

        @SerializedName("profile_image")
        val profile_image: String,

        @SerializedName("nif_number")
        val nif_number: String,

        @SerializedName("about_us")
        val about_us: String,

        @SerializedName("company_id")
        val company_id: Int,

        @SerializedName("service_ids")
        val service_ids: Int,

        @SerializedName("social_type")
        val social_type: String,

        @SerializedName("social_id")
        val social_id: String,

        @SerializedName("role")
        val role: String,

        )
}