package com.assistant.top.service.provider.extension

import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("rectDrawable", "rectColor", requireAll = false)
fun View.setRectDrawable(radius: Float = 15F, rectColor: Int) {
    val drawable =
        GradientDrawable(
            GradientDrawable.Orientation.TL_BR,
            intArrayOf(rectColor, rectColor)
        )
    drawable.shape = GradientDrawable.RECTANGLE
    drawable.cornerRadius = radius
    this.background = drawable
}

@BindingAdapter("circularDrawable", requireAll = false)
fun View.setCircularDrawable(color: Int) {
    val drawable =
        GradientDrawable(GradientDrawable.Orientation.TL_BR, intArrayOf(color, color))
    drawable.shape = GradientDrawable.OVAL
    this.background = drawable
}

@BindingAdapter(
    "borderRectDrawable",
    "strokeWidth",
    "strokeColor",
    "startColor",
    "endColor",
    requireAll = false
)
fun View.setBorderRectDrawable(
    radius: Float,
    strokeWidth: Int? = null,
    strokeColor: Int? = null,
    startColor: Int? = null,
    endColor: Int? = null
) {
    val drawable =
        GradientDrawable(
            GradientDrawable.Orientation.RIGHT_LEFT,
            intArrayOf(startColor ?: Color.TRANSPARENT, endColor ?: Color.TRANSPARENT)
        )
    drawable.shape = GradientDrawable.RECTANGLE
    drawable.setStroke(strokeWidth ?: 1, strokeColor ?: Color.WHITE)
    drawable.cornerRadius = radius
    this.background = drawable
}


