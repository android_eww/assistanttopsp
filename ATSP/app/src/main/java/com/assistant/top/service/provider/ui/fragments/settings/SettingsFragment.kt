package com.assistant.top.service.provider.ui.fragments.settings

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.BuildConfig
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.FragmentSettingsBinding
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarEntry
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class SettingsFragment : BaseFragment<FragmentSettingsBinding, SettingsViewModel>() {

    override val layoutId: Int get() = R.layout.fragment_settings
    override val bindingVariable: Int get() = BR.viewmodel
    var sales: ArrayList<BarEntry> = ArrayList()
    var month: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        binding.curveChart.setTouchEnabled(false)
//
//        binding.curveChart.setViewPortOffsets(0f, 20f, 0f, 0f);
//        binding.curveChart.setBackgroundColor(Color.rgb(255, 255, 255));
//
//        binding.curveChart.setTouchEnabled(true)
//
//         binding.curveChart.setDragEnabled(true)
//         binding.curveChart.setScaleEnabled(true)
//
//        binding.curveChart.setPinchZoom(false)
//        binding.curveChart.setDrawGridBackground(true)
//
//        val xAxis: XAxis = binding.curveChart.getXAxis()
//        xAxis.isEnabled = true
//        xAxis.position = XAxis.XAxisPosition.TOP_INSIDE
//        xAxis.textSize = 10f
//        xAxis.textColor = Color.BLACK
//        xAxis.setDrawAxisLine(true)
//        xAxis.setDrawGridLines(false)
//
//
//        val yAxis: YAxis = binding.curveChart.getAxisLeft()
//
//        yAxis.setLabelCount(12, false)
//        yAxis.textColor = Color.BLACK
//        yAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART)
//        yAxis.setDrawGridLines(true)
//        yAxis.setAxisLineColor(Color.BLACK)
//
//        binding.curveChart.getAxisRight().setEnabled(false)
//        binding.curveChart.getLegend().setEnabled(false)
//        binding.curveChart.animateXY(2000, 2000)
//
//        binding.curveChart.invalidate()
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.versionName.text = getString(R.string.version).plus(BuildConfig.VERSION_NAME)
    }

    override fun setupObservable() {

    }





}
