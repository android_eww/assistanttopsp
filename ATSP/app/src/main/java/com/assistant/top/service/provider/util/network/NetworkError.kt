package com.assistant.top.service.provider.util.network

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NetworkError(

    @Expose
    @SerializedName("code")
    var statusCode: Int? = null,

    @Expose
    @SerializedName("code")
    var message: String? = null
) {

    fun NetworkError(statusCode: Int, message: String) {
        this.message = message
        this.statusCode = statusCode

    }
}