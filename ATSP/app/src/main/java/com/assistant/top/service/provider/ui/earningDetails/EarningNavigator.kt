package com.assistant.top.service.provider.ui.earningDetails

interface EarningNavigator {

    fun onItemClick(position : Int)
}