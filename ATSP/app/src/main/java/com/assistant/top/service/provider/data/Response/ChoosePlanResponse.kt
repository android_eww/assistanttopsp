package com.assistant.top.service.provider.data.Response

import com.google.gson.annotations.SerializedName

class ChoosePlanResponse (

    @SerializedName("plans")
    var plansList: ArrayList<plansData>,

    ) : BaseResponse() {

    data class plansData(
        @SerializedName("id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("price")
        val price: String,

        @SerializedName("days")
        val days: String,


        )
}