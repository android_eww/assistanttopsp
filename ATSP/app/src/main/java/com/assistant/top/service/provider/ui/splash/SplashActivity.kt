package com.assistant.top.service.provider.ui.splash

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import com.airbnb.lottie.utils.Utils
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivitySplashBinding
import com.assistant.top.service.provider.extension.hideKeyboard
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.extension.startNewActivityWithClearTop
import com.assistant.top.service.provider.ui.home.HomeActivity
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.network.AppConstants.SharedPrefKey.Companion.USER_PREF
import com.assistant.top.service.provider.util.network.AppConstants.SharedPrefKey.Companion.USER_PREF_KEY
import com.assistant.top.service.provider.util.network.Resource
import com.assistentetop.customer.utils.alertDialog.alertDialog
import com.assistentetop.customer.utils.alertDialog.showInternetDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    override val layoutId: Int
        get() = R.layout.activity_splash
    override val bindingVariable: Int
        get() = BR.viewmodel

    private val TAG = "SplashActivity"
    private var isInternetConnected: Boolean = true
    private var isDialogDismiss = true
    private var isTimerFinish = false
    val isNetworkAvailable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel.setLanguageObject(language!!)

        val timer = object : CountDownTimer(4000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }

            override fun onFinish() {
                isTimerFinish = true
                /* if (!userPreference.getUserId().isEmpty()){
                     startNewActivity(HomeActivity::class.java,finish = true)
                 }*/
            }
        }
        timer.start()
        mViewModel.initApi()
        mViewModel.lagunageLabel()

    }

    override fun setupObservable() {
        mViewModel.getInitObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    Log.e(TAG, "on success=>${it.message}")

                    it.data!!.let {
                        mViewModel.checkApiFlags(it)
                    }
                }

                Resource.Status.ERROR -> {
                    Log.e(TAG, "on error=>${it.message}")
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            alertDialog(
                                message = if (checkIsConnectionReset(it.code)) {
                                    getString(R.string.connection_reset)
                                } else {
                                    message
                                }
                            )
                        }
                    }
                }

                Resource.Status.LOADING -> {
                    Log.e(TAG, "loading=>${it.message}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e(TAG, "no internet=>${it.message}")
                    isInternetConnected = false
                    showInternetDialog(false)
                }

            }
        })

        mViewModel.geLaunchHomeObservable().observe(this, {
            launchHome(true)
        })

        mViewModel.geLaunchLoginObservable().observe(this, {
            launchHome(false)
        })

        mViewModel.getLanguageLabelObservable().observe(this) {
            lagunageLabel()
        }

        mViewModel.getOptionUpdateObservable().observe(this, {
            isDialogDismiss = false
            alertDialog(title = getString(R.string.update_available),
                message = it,
                negativeBtnText = getString(R.string.no),
                positiveBtnText = getString(R.string.yes),
                positiveClick = {
                    isDialogDismiss = true
                    launchPlayStore()
                },
                negativeClick = {
                    isDialogDismiss = true
                    mViewModel.goToNextScreen()
                })
        })

        mViewModel.getCompulsoryUpdateObservable().observe(this, {
            isDialogDismiss = false
            alertDialog(
                message = it,
                positiveBtnText = getString(R.string.update),
                positiveClick = {
                    isDialogDismiss = true
                    launchPlayStore()
                })

        })

        mViewModel.getMaintenanceObservable().observe(this, {
            isDialogDismiss = false
            alertDialog(title = getString(R.string.under_maintenance),
                message = it,
                positiveBtnText = getString(R.string.ok),
                positiveClick = {
                    isDialogDismiss = true
                    goBack()
                })
        })
    }

    private fun lagunageLabel() {
        AppLog.e(TAG, "lagunageLabel() ")

        mViewModel.getLanguageLabelObservable().observe(this, {

            AppLog.e(TAG, "it.status ")

            when (it.status) {
                Resource.Status.SUCCESS -> {

                    AppLog.e(TAG, "success_label")
                }

                Resource.Status.ERROR -> {
                    Log.e(TAG, "error_label ${it.message} ")
                }

                Resource.Status.LOADING -> {
                    Log.e(TAG, "LOADING  ${it.status}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {
                    Log.e(TAG, "NO_INTERNET_CONNECTION ${it.status}")
                    isInternetConnected = false
                    showInternetDialog(false)
                }
            }

        })
    }

    private fun launchHome(isLaunchHome: Boolean) {
        Handler().postDelayed(
            {
                if (isTimerFinish) {
                    AppLog.e("isTimerFinish", "isTimerFinish::::-> $isTimerFinish")

                    if (isLaunchHome) {
                        AppLog.e("launchHome", "Line => 190")
                        startNewActivity(HomeActivity::class.java)
                        finish()
                    } else {
                        AppLog.e("launchHome", "Line => 200")
                        startNewActivity(LoginActivity::class.java)
                        finish()
                    }
                    finish()
                } else {
                    launchHome(isLaunchHome)
                }
            },
            800
        )
    }

    private fun launchPlayStore() {
        val packageName = packageName
        AppLog.d("TAG", "launchPlayStore   packageName  $packageName")

        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        } catch (actNotFoundExp: ActivityNotFoundException) {
            actNotFoundExp.printStackTrace()
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                    )
                )
            } catch (e: Exception) {
                AppLog.d(TAG, "goToPlayStore called: play store exception e = \$e ")
            }
        }

        finish()
    }
}