package com.assistant.top.service.provider.ui.view

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.ViewToolbarBinding

class MVToolbar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {
    val binding: ViewToolbarBinding =
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_toolbar, this, true)
    private var onBackListener: (() -> Unit?)? = null
    private var menuOneClickListener: (() -> Unit?)? = null
    private var menuTwoClickListener: (() -> Unit?)? = null


    fun setToolbarBackListener(listener: () -> (Unit)) {
        this.onBackListener = listener
    }

    fun setMenuOneClickListener(listener: () -> Unit) {
        this.menuOneClickListener = listener
    }

    fun setMenuTwoClickListener(listener: () -> Unit) {
        this.menuTwoClickListener = listener
    }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MVToolBar)
        val title = a.getString(R.styleable.MVToolBar_title)
        val fontCode = a.getInt(R.styleable.MVToolBar_fontCode, 0)
        setFont(binding,fontCode,context)


        binding.ivMenu1.setImageDrawable(a.getDrawable(R.styleable.MVToolBar_menu1))
        binding.ivNotification.setImageDrawable(a.getDrawable(R.styleable.MVToolBar_ivNotification))
        binding.ivBack.setImageDrawable(a.getDrawable(R.styleable.MVToolBar_menu3))


        val isTitleVisible = a.getBoolean(R.styleable.MVToolBar_isTitleVisible, false)
        val isBackArrowVisible = a.getBoolean(R.styleable.MVToolBar_isBackArrowVisible, false)
        val isNotificationIcon = a.getBoolean(R.styleable.MVToolBar_isNotificationVisible, false)

        binding.tvTitle.textSize = getTextSize(a)

        val textColor = a.getInt(R.styleable.MVToolBar_textColor, R.color.white)
        binding.tvTitle.setTextColor(textColor)

        Log.e("title", "== $title")
        Log.e("isTitleVisible", "== $isTitleVisible")

        binding.tvTitle.text = title
        if (isBackArrowVisible) {
            binding.ivBack.visibility = View.VISIBLE
        } else {
            binding.ivBack.visibility = View.GONE
        }

        if (isNotificationIcon) {
            binding.ivNotification.visibility = View.VISIBLE
        } else {
            binding.ivNotification.visibility = View.INVISIBLE
        }

        binding.ivBack.setOnClickListener {
            if (onBackListener != null) {
                onBackListener?.invoke()
                return@setOnClickListener
            }
            if (context is Activity) {
                context.onBackPressed()
            }
        }
        binding.ivMenu1.setOnClickListener {
            menuOneClickListener?.invoke()
        }
        binding.ivNotification.setOnClickListener {
            menuTwoClickListener?.invoke()
        }
    }

    private fun setFont(binding: ViewToolbarBinding, fontCode: Int, context: Context) {

        when(fontCode){
            1-> {
                binding.tvTitle.typeface = ResourcesCompat.getFont(context, R.font.aileron_black)
            } else -> {
                binding.tvTitle.typeface = ResourcesCompat.getFont(context, R.font.aileron_regular)
            }
        }
    }

    private fun getTextSize(data: TypedArray): Float {
        var size = data.getString(R.styleable.MVToolBar_size)
        if (size == null || size == "")
            size = "20"
        return size.toFloat()
    }
}