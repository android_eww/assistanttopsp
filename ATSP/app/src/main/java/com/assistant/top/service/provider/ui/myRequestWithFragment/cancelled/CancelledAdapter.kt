package com.assistant.top.service.provider.ui.myRequestWithFragment.cancelled

import android.annotation.SuppressLint
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.PendingServiceListItemBinding
import com.assistant.top.service.provider.extension.setRectDrawable
import com.assistant.top.service.provider.utility.Constants

class CancelledAdapter(val requireActivity: FragmentActivity) :
    RecyclerView.Adapter<CancelledAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: PendingServiceListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.pending_service_list_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        binding.btnStartService.binding.button.text =
            requireActivity.getString(R.string.cancelled)
        binding.btnStartService.binding.button.setRectDrawable(
            90f,
            ContextCompat.getColor(requireActivity, R.color.red)
        )


        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000){
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
        }
    }


    override fun getItemCount(): Int {
        return 5
    }

    class MenuHolder(var binding: PendingServiceListItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}