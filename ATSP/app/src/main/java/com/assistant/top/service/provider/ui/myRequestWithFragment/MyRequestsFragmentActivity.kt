package com.assistant.top.service.provider.ui.myRequestWithFragment

import android.os.Bundle
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityMyRequestsFragemntsBinding
import com.assistant.top.service.provider.utility.IntentKey
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MyRequestsFragmentActivity :
    BaseActivity<ActivityMyRequestsFragemntsBinding, MyRequestFragmentViewModel>(),
    MyRequestFragmentNavigator {

    private var position: Int = 0
    private lateinit var tabAdapter: TabSelectionAdapter
    private lateinit var adapterViewPager: MyPagerAdapter
    override val layoutId: Int get() = R.layout.activity_my_requests_fragemnts
    override val bindingVariable: Int get() = BR._all

    var tabList = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initt()

        binding.mViewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                tabAdapter.scrollTo(position)
                binding.rvTab.scrollToPosition(position)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
    }

    private fun initt() {
        mViewModel.setNavigator(this)

        tabList.add(getString(R.string.pending_services))
        tabList.add(getString(R.string.in_progress_services))
        tabList.add(getString(R.string.completed_services))
        tabList.add(getString(R.string.cancelled_services))

        if (intent != null) {
            position = intent.getIntExtra(IntentKey.POSITION, 0)
        }

        tabAdapter = TabSelectionAdapter(mViewModel.getNavigator(), tabList, position)
        binding.rvTab.adapter = tabAdapter

        adapterViewPager = MyPagerAdapter(supportFragmentManager)
        binding.mViewPager.adapter = adapterViewPager

        binding.rvTab.scrollToPosition(position)
        onItemClick(position)
    }

    override fun onItemClick(position: Int) {
        binding.mViewPager.currentItem = position
    }

    override fun onBackPressed() {
        finish()
    }

    override fun setupObservable() {

    }
}