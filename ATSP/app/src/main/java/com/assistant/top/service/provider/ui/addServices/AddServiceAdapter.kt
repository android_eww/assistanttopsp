package com.assistant.top.service.provider.ui.addServices

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.data.Response.AddServicesResponse
import com.assistant.top.service.provider.databinding.AddServiceItemListSecondBinding
import com.assistant.top.service.provider.extension.TAG
import com.assistant.top.service.provider.extension.loadImage
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.utility.Constants


class AddServicesAdapter(
    var servicesList: ArrayList<AddServicesResponse.ServiceTypeData>,

    ) : RecyclerView.Adapter<AddServicesAdapter.MenuHolder>() {

    private var selectedPosition = -1
    lateinit var context: Context
    lateinit var binding: AddServiceItemListSecondBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.add_service_item_list_second,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {


        val list = servicesList[position]
        setData(holder, list.name, list.image)

        AppLog.e(TAG,"id => ${list.id}")
        AppLog.e(TAG,"name => ${list.name}")
        AppLog.e(TAG,"image => ${list.image}")
        AppLog.e(TAG,"image_yellow => ${list.yellow_image}")

        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            selectedPosition = position
            notifyDataSetChanged()
        }
    }

    private fun setData(holder: MenuHolder, serviceName: String, resource: String) {
        holder.binding.serviceIcon.loadImage(resource)
        holder.binding.serviceName.text = serviceName


        AppLog.e(TAG,"names=> ${holder.binding.serviceName.text}")
        AppLog.e(TAG,"images=> ${holder.binding.serviceIcon.loadImage(resource)}")

    }


    override fun getItemCount(): Int {
        //return 5
        return servicesList.size
    }

    class MenuHolder(var binding: AddServiceItemListSecondBinding) :
        RecyclerView.ViewHolder(binding.root)
}