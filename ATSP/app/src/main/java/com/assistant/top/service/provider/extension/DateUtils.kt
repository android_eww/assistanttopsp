package com.assistant.top.service.provider.extension

import java.text.SimpleDateFormat
import java.util.*

fun String.getDateFormat(
    inputFormat: String = "yyyy-MM-dd hh:mm:ss",
    outputFormat: String = "yyyy-MM-dd"
): String {
    val inputSimpleDateFormat = SimpleDateFormat(inputFormat, Locale.ENGLISH)
    val outputDateFormat = SimpleDateFormat(outputFormat, Locale.ENGLISH)
    return outputDateFormat.format(inputSimpleDateFormat.parse(this)).toString()
}