package com.assistant.top.service.provider.ui.forgotPassword

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.WindowManager
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityForgotPasswordBinding
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.login.LoginActivity
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.Resource
import com.assistentetop.customer.utils.alertDialog.showErrorMsg502
import com.assistentetop.customer.utils.alertDialog.showSuccessAlert
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotPassword : BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>(),
    ForgotNavigator {
    override val layoutId: Int get() = R.layout.activity_forgot_password
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.setLanguageObject(language!!)
        mViewModel.setNavigator(this)

        getWindow().setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        binding.ivBack.setOnClickListener {
            finish()
        }
        binding.btnSend.setCommonButtonListener {
            mViewModel.isforgotPwd()
        }
    }

    override fun showLoaderButton() {
        binding.btnSend.setLoading(true, userPreference)

    }

    override fun setupObservable() {
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(this, it)
        })
        mViewModel.getLogoutObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    //hideLoaderDialog()
                    binding.btnSend.setLoading(false, userPreference)
                    /* showSuccessMsg(msg = it.data!!.message)
                     startNewActivity(LoginActivity::class.java)*/

                    it.data?.message?.let { message ->
                        showSuccessAlert(language?.getLanguage(message) ?: "")

                        val timer = object : CountDownTimer(2500, 1000) {
                            override fun onTick(millisUntilFinished: Long) {

                            }

                            override fun onFinish() {
                                startNewActivity(LoginActivity::class.java, finish = true)
                            }
                        }
                        timer.start()
                    }
                }

                Resource.Status.ERROR -> {
                    //hideLoaderDialog()
                    binding.btnSend.setLoading(false, userPreference)
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            showErrorMsg502(it.code, language?.getLanguage(message) ?: "")
                            Log.e("TAG", "On Error ${it.message} ")
                        }
                    }
                }

                Resource.Status.LOADING -> {
                    // showLoaderDialog()
//                    viewDataBinding.btnSend.(true)
                    Log.e("TAG", "Loading =>  ${it.message}")

                }


            }
        })
    }


}