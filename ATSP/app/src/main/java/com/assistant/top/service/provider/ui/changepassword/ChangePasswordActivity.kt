package com.assistant.top.service.provider.ui.changepassword

import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.util.Log
import androidx.databinding.library.baseAdapters.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityChangePasswordBinding
import com.assistant.top.service.provider.util.Validation
import com.assistant.top.service.provider.util.network.Resource
import com.assistant.top.service.provider.utility.Constants
import com.assistentetop.customer.utils.alertDialog.showErrorMsg502
import com.assistentetop.customer.utils.alertDialog.showInternetDialog
import com.assistentetop.customer.utils.alertDialog.showSuccessAlert
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ChangePasswordActivity :
    BaseActivity<ActivityChangePasswordBinding, ChangePasswordViewModel>(),
    ChangePasswordNavigator {

    override val layoutId: Int
        get() = R.layout.activity_change_password
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.setNavigator(this)

        binding.btnSubmit.setCommonButtonListener {

            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setCommonButtonListener
            }

            Constants.mLastClickTime = SystemClock.elapsedRealtime()
            mViewModel.changePassword()
            //onBackPressed()
        }

    }

    override fun gotoprofile() {
        finish()
    }

    override fun showLoader() {
        binding.btnSubmit.setLoading(true, userPreference)
    }

    override fun setupObservable() {
        mViewModel.getValidationStatus().observe(this, {
            Validation.showMessageDialog(this, it)
        })

        mViewModel.getChangePwdObservable().observe(this, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.btnSubmit.setLoading(false, userPreference)
                    Log.e("TAG", "On Success ${it.data?.message} ")
                    it.message?.let { message ->
                        showSuccessAlert(language?.getLanguage(message) ?: "")
                        clearAllData()


                        val timer = object : CountDownTimer(2500, 1000) {
                            override fun onTick(millisUntilFinished: Long) {

                            }

                            override fun onFinish() {
                                onBackPressed()
                            }
                        }
                        timer.start()
                    }
                }

                Resource.Status.ERROR -> {

                    binding.btnSubmit.setLoading(false, userPreference)
                    if (!checkIsSessionOut(it.code)) {
                        it.message?.let { message ->
                            showErrorMsg502(it.code, language?.getLanguage(message) ?: "")
                        }
                        Log.e("TAG", "On Error ${it.message} ")
                    }
                }


                Resource.Status.LOADING -> {

                    Log.e("TAG", "Loading =>  ${it.message}")
                }

                Resource.Status.NO_INTERNET_CONNECTION -> {

                    binding.btnSubmit.setLoading(false, userPreference)
                    Log.e("noInternetActivity", "here.....")
                    showInternetDialog(
                        false
                    )
                }
            }
        })
    }

    private fun clearAllData() {
        var password = ""
        binding.etCurrentPwd.setText(password)
        binding.etNewPassword.setText(password)
        binding.etConfirmPwd.setText(password)
    }
}