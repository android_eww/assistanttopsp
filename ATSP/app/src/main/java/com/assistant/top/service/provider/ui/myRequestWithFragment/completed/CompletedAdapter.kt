package com.assistant.top.service.provider.ui.myRequestWithFragment.completed

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.SystemClock
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.PendingServiceListItemBinding
import com.assistant.top.service.provider.extension.setRectDrawable
import com.assistant.top.service.provider.extension.startNewActivity
import com.assistant.top.service.provider.ui.service_details.ServiceDetailsActivity
import com.assistant.top.service.provider.utility.Constants
import com.assistant.top.service.provider.utility.IntentKey

class CompletedAdapter(val requireActivity: FragmentActivity) : RecyclerView.Adapter<CompletedAdapter.MenuHolder>() {

    lateinit var context: Context
    lateinit var binding: PendingServiceListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHolder {

        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.pending_service_list_item,
            parent,

            false
        )

        return MenuHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: MenuHolder, @SuppressLint("RecyclerView") position: Int) {

        binding.btnStartService.binding.button.text =
            requireActivity.getString(R.string.completed)
        binding.btnStartService.binding.button.setRectDrawable(
            90f,
            ContextCompat.getColor(requireActivity, R.color.green)
        )

        holder.itemView.setOnClickListener {
            if (SystemClock.elapsedRealtime() - Constants.mLastClickTime < 1000) {
                return@setOnClickListener
            }
            Constants.mLastClickTime = SystemClock.elapsedRealtime()

            val bundle = Bundle()
            bundle.putInt(IntentKey.POSITION, 2)
            requireActivity.startNewActivity(ServiceDetailsActivity::class.java, bundle = bundle)
        }

    }

    override fun getItemCount(): Int {
        return 5
    }

    class MenuHolder(var binding: PendingServiceListItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}