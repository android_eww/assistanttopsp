package com.assistant.top.service.provider.language

import android.content.Context
import android.content.SharedPreferences
import org.json.JSONObject
import org.json.JSONException

class Language(context: Context) {
    private val prefs: SharedPreferences? = context.getSharedPreferences(
        SHARED_PREFERENCE_ASSISTANTETOP_SP_LANGUAGE,
        Context.MODE_PRIVATE
    )
    var languageCode: String?
        get() = prefs!!.getString(PREF_LANGUAGE_CODE, null)
        set(code) {
            prefs!!.edit().putString(PREF_LANGUAGE_CODE, code).apply()
        }

    fun getLanguage(key: String?): String {
        try {
            if (prefs != null) {
                val pref = prefs.getString(PREF_SAVE_LANGUAGE, null)
                if (pref != null && pref != "") {
                    val jsonObject = JSONObject(pref)
                    return jsonObject.getString(key)
                }
                return ""
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return ""
    }

    fun setLanguage(`object`: String?) {
        prefs!!.edit().putString(PREF_SAVE_LANGUAGE, `object`).apply()
    }

    companion object {
        const val PREF_SAVE_LANGUAGE = "save_language_lable_service"
        var SHARED_PREFERENCE_ASSISTANTETOP_SP_LANGUAGE = "assistant_top_service_provider"
        private var instance: Language? = null
        const val PREF_LANGUAGE_CODE = "language_code_service"
        fun newInstance(context: Context): Language? {
            if (instance == null) {
                instance = Language(context)
            }
            return instance
        }
    }

}