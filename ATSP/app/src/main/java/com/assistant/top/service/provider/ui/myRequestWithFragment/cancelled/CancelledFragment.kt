package com.assistant.top.service.provider.ui.myRequestWithFragment.cancelled

import android.os.Bundle
import android.view.View
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.PendingTabBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CancelledFragment : BaseFragment<PendingTabBinding, CancelledViewModel>() {

    override val layoutId: Int get() = R.layout.pending_tab
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvPendingList.adapter = CancelledAdapter(requireActivity())

    }

    override fun setupObservable() {

    }

}