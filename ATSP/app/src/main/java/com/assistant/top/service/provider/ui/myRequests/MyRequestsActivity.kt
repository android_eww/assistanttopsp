package com.assistant.top.service.provider.ui.myRequests

import android.os.Bundle
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseActivity
import com.assistant.top.service.provider.databinding.ActivityMyRequestsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MyRequestsActivity : BaseActivity<ActivityMyRequestsBinding, MyRequestViewModel>(), MyRequestNavigator {

    override val layoutId: Int get() = R.layout.activity_my_requests
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding.rvMyRequestsList.adapter = MyRequestAdapter(this)
    }

    override fun setupObservable() {

    }


}