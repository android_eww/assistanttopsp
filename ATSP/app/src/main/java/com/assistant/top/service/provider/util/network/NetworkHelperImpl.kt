package com.assistant.top.service.provider.util.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.assistant.top.service.provider.R
import retrofit2.HttpException
import java.net.ConnectException
import javax.inject.Inject

class NetworkHelperImpl @Inject constructor(val context: Context) : NetworkHelper {
    override fun isNetworkConnected(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                return if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    true
                } else capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
            }
        }
        return false
    }

    override fun castToNetworkError(throwable: Throwable?): NetworkError {
        if (throwable is ConnectException) {
            return NetworkError(0, context.getString(R.string.server_connection_error))
        }

        return if (throwable !is HttpException) {
            NetworkError(-1, context.getString(R.string.not_a_http_exception_message))
        } else NetworkError(throwable.code(), throwable.message())
    }
}