package com.assistant.top.service.provider.ui.edit_service.general

import android.os.Bundle
import android.view.View
import com.assistant.top.service.provider.BR
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.base.BaseFragment
import com.assistant.top.service.provider.databinding.PendingTabBinding
import com.assistant.top.service.provider.ui.edit_service.appointment.AppointmentViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GeneralFragment : BaseFragment<PendingTabBinding, AppointmentViewModel>() {

    override val layoutId: Int get() = R.layout.pending_tab
    override val bindingVariable: Int get() = BR.viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvPendingList.adapter = GeneralAdapter(requireActivity())

    }

    override fun setupObservable() {

    }

}