package com.assistant.top.service.provider.ui.view

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.setPadding
import androidx.databinding.DataBindingUtil
import com.assistant.top.service.provider.R
import com.assistant.top.service.provider.databinding.CommonButtonWithLoaderBinding
import com.assistant.top.service.provider.extension.gone
import com.assistant.top.service.provider.extension.setRectDrawable
import com.assistant.top.service.provider.extension.visible
import com.assistant.top.service.provider.prefs.UserPreference
import com.assistant.top.service.provider.util.network.AppConstants.SharedPrefKey.Companion.USER_PREF_BUTTON_TEXT
import com.assistant.top.service.provider.utility.Constants.logMessage

@SuppressLint("CustomViewStyleable")
class CustomButtomProgressBar @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : FrameLayout(context, attrs, defStyle) {
    val binding: CommonButtonWithLoaderBinding =
        DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.common_button_with_loader,
            this,
            true
        )

    private var onClickListener: (() -> Unit?)? = null


    fun setCommonButtonListener(listener: () -> (Unit)) {
        this.onClickListener = listener
    }

    init {

        val data = context.obtainStyledAttributes(attrs, R.styleable.ButtonProgress)

        data.resources

        val padding = data.getInt(R.styleable.ButtonProgress_padding, 30)
        binding.loader.setPadding(padding)

        val text = data.getString(R.styleable.ButtonProgress_text)
        binding.button.text = text

        val textAllCaps = data.getString(R.styleable.ButtonProgress_textAllCaps)
        binding.button.isAllCaps = false

        binding.button.textSize = data.getInt(R.styleable.ButtonProgress_tSize, 20).toFloat()

        val textColor = data.getInt(R.styleable.ButtonProgress_buttonTextColor, R.color.white)
        binding.button.setTextColor(textColor)

        val fontCode = data.getInt(R.styleable.ButtonProgress_styleFont, 2)
        Log.e("fontCode", "fontCode = $fontCode")
        setFont(binding, fontCode, context)

        binding.button.setRectDrawable(
            data.getString(R.styleable.ButtonProgress_radius)!!.toFloat(),
            data.getInt(R.styleable.ButtonProgress_bgColor, R.color.yellow)
        )

        binding.button.setOnClickListener {
            if (onClickListener != null) {
                onClickListener?.invoke()
                return@setOnClickListener
            }
        }
    }

    @SuppressLint("ResourceType")
    fun setLoading(loading: Boolean, userPreference: UserPreference) {

        if (loading) {
            userPreference.saveUserSession(USER_PREF_BUTTON_TEXT,binding.button.text.toString())
            binding.button.text = ""
            binding.loader.visible()
            binding.loader.bringToFront()

        } else {
            binding.loader.gone()
            binding.button.text = userPreference.getButtonText()
        }
        Log.e("buttonText","buttonText = ${userPreference.getButtonText()}")
    }

    fun setLoadingProgress(loading: Boolean, userPreference: UserPreference) {
        if (loading) {
            userPreference.setButtonText(binding.button.text.toString())
            binding.button.text = ""
            binding.loader.visible()
        } else {
            binding.button.text = userPreference.getProgressButtonText()
            binding.loader.gone()
        }
    }

    private fun setFont(binding: CommonButtonWithLoaderBinding, fontCode: Int, context: Context) {

        when (fontCode) {
            1 -> {
                binding.button.typeface = ResourcesCompat.getFont(context, R.font.aileron_black)
            }
            2 -> {
                binding.button.typeface = ResourcesCompat.getFont(context, R.font.aileron_bold)
            }
            else -> {
                binding.button.typeface = ResourcesCompat.getFont(context, R.font.aileron_regular)
            }
        }
    }

    @SuppressLint("LongLogTag")
    fun setOnTouchListener() {
        Log.e("AssistantTopServiceProvider", "onClicked")
    }

}
