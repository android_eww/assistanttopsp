package com.assistant.top.service.provider.ui.forgotPassword

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assistant.top.service.provider.base.BaseViewModel
import com.assistant.top.service.provider.data.Response.BaseResponse
import com.assistant.top.service.provider.util.AppLog
import com.assistant.top.service.provider.util.ValidationStatus
import com.assistant.top.service.provider.util.isEmailValid
import com.assistant.top.service.provider.util.network.API_CONSTANTS
import com.assistant.top.service.provider.util.network.NetworkService
import com.assistant.top.service.provider.util.network.NetworkUtils
import com.assistant.top.service.provider.util.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class ForgotPasswordViewModel @Inject constructor(
    val context: Context,
    var networkService: NetworkService
) : BaseViewModel<ForgotNavigator>(){

    var emailAddress: String = ""

    private val forgotObservable: MutableLiveData<Resource<BaseResponse>> =
        MutableLiveData()

    fun getLogoutObservable(): LiveData<Resource<BaseResponse>> {
        return forgotObservable
    }

    fun isforgotPwd() {
        var response: Response<BaseResponse>? = null

        if (NetworkUtils.isNetworkConnected(context)) {
            if (validationForgot()) {

                getNavigator()?.showLoaderButton()

                viewModelScope.launch {
                    forgotObservable.value = Resource.loading(null)
                    withContext(Dispatchers.IO) {
                        val forgotPassRequest: MutableMap<String, String> = HashMap()
                        forgotPassRequest[API_CONSTANTS.WEB_PARAM_EMAIL_OR_PHONE] = emailAddress
                        response = networkService.forgotPwd(forgotPassRequest)
                    }

                    withContext(Dispatchers.Main) {
                        response.run {
                            forgotObservable.value = baseDataSource.getResult { this!! }
                        }
                    }
                }
            }
        } else {
            viewModelScope.launch {
                withContext(Dispatchers.Main) {
                    forgotObservable.value = Resource.noInternetConnection(null)
                }
            }
        }
    }


    fun validationForgot(): Boolean {

        when {
            emailAddress.isEmpty() -> {
                sendError(ValidationStatus.EMPTYEMAIL)
                AppLog.e("loginParam", "emailAddress $emailAddress")
            }

            !emailAddress.trim().isEmailValid() -> {
                sendError(ValidationStatus.InvalidEmail)
                AppLog.e("loginParam", "emailAddressvalid $emailAddress")

            }
            else -> {
                return true
            }
        }

        return false
    }

    private fun sendError(error: ValidationStatus) {
        getValidationStatus().postValue(error)
    }

}