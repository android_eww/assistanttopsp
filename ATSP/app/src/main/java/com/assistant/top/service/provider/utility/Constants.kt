package com.assistant.top.service.provider.utility

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.assistant.top.service.provider.R


object Constants {

    val TAG ="AssistantTopServiceProvider"
    var CURRENT_POSITION = 0
    var mLastClickTime: Long = 0


    fun onClickView(
        rcView: RecyclerView,
        arrow: ImageView
    ) {

        if (arrow.isSelected) {
            arrow.setImageResource(R.drawable.ic_arrow_down)
            arrow.isSelected = false
            rcView.visibility = View.GONE

        } else {

            arrow.setImageResource(R.drawable.ic_arrrow_up)
            arrow.isSelected = true
            rcView.visibility = View.VISIBLE
        }
    }

    @SuppressLint("LongLogTag")
    fun logMessage(tag: String = "AssistantTopServiceProvider", msg: String) {
        Log.e(TAG + tag, "= $msg")
    }

}