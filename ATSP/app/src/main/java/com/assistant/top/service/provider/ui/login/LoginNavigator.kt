package com.assistant.top.service.provider.ui.login

interface LoginNavigator {

    fun gotoForgotPassword()
    fun gotoSignUp()
    fun goToHome()
    fun addToCart()
    fun showLoader()

}