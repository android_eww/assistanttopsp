package com.assistant.top.service.provider.ui.choosePlan

interface ChoosePlanNavigator {

    fun onItemClick()
    fun showLoader()

}